package com.automation.testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.automation.pages.LoginPage;
import com.automation.utility.BrowserFactory;

public class LoginTestCRM{
	
	WebDriver driver;
	
	@Test
	public void loginApp() {
		
		driver = BrowserFactory.startApplication(driver, "Chrome", "https://ui.freecrm.com/");
		//System.out.println(driver.getTitle());
		
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.logintoCRM("garlapati99@gmail.com", "Hesdidji99");
		
		BrowserFactory.quitBrowser(driver);
		
		
	}

}
