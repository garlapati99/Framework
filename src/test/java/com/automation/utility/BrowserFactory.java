package com.automation.utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserFactory {
	
	//public WebDriver driver;

	
	public static WebDriver startApplication(WebDriver driver, String browserName, String appURL) {
		
		if(browserName.equals("Chrome")) {
			
			System.setProperty("webdriver.chrome.driver", "lib/chromedriver");
			driver = new ChromeDriver();
			
		}
		else if (browserName.contains("Firefox")) {
			System.out.println("Firefox driver");
		}
		else if(browserName.contains("IE")) {
			System.out.println("Currently no IE supporteds");
		}
		else
		{
			System.out.println("We dont support your browser");
		}
			
		driver.manage().timeouts().pageLoadTimeout(30,  TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(appURL);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}
	
	public static void quitBrowser(WebDriver driver) {
		driver.close();
	}
	
}
